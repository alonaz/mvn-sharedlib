This project demonstrates maven capability to publish artifacts to artifactory repository. It builds a simple jar, and publishes it to the artifactory mentined in the pom.xml file.
In addition it shows how to use the versions-maven-plugin to update your application verstion numbers from your CI server
Usage:

Prepare
-------
0. make sure you have a running artifactory instance locally or remotely
1. make sure you have settings.xml form maven updated accordingly as attatched to this project

Setup version number from your CI server
--------------------
0. run mvn versions:set -DnewVersion=1.0.3
1. use the CI build number as the newVersion value

Publish to artifactory
----------------------
0. run mvn deploy
1. go to http://localhost:8081/artifactory/webapp/home.html and check under the repository libs-release-local you see your published jar with the correct version

